//
//  CategoryCell.swift
//  Categories
//
//  Created by Gray Zhen on 9/26/17.
//  Copyright © 2017 GrayStudio. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryCellImage: UIImageView!
    @IBOutlet weak var categoryCellTitle: UILabel!
    
    func updateViews(category: Category) {
        categoryCellImage.image = UIImage(named: category.imageName)
        categoryCellTitle.text = category.title
    }

}
