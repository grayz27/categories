//
//  ProductCell.swift
//  Categories
//
//  Created by Gray Zhen on 9/27/17.
//  Copyright © 2017 GrayStudio. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var imageName: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    
    func updateView(product: Product){
        imageName.image = UIImage(named: product.imageName)
        title.text = product.title
        price.text = product.price
    }
    
}
