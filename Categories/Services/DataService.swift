//
//  DataService.swift
//  Categories
//
//  Created by Gray Zhen on 9/26/17.
//  Copyright © 2017 GrayStudio. All rights reserved.
//

import Foundation

class DataService{
    
    static let instance = DataService()
    
    private let catetories = [
        Category(title: "SHIRTS", imageName: "shirts.png"),
        Category(title: "HOODIES", imageName: "hoodies.png"),
        Category(title: "HATS", imageName: "hats.png"),
        Category(title: "DIGITAL", imageName: "digital.png")
    ]
    
    private let hats = [
        Product(title: "Hat #1", price: "$18", imageName: "hat01.png"),
        Product(title: "Hat #2", price: "$22", imageName: "hat02.png"),
        Product(title: "Hat #3", price: "$22", imageName: "hat03.png"),
        Product(title: "Hat #4", price: "$20", imageName: "hat04.png")
    ]
    
    private let hoodies = [
        Product(title: "Hoodie #1", price: "$32", imageName: "hoodie01.png"),
        Product(title: "Hoodie #2", price: "$32", imageName: "hoodie02.png"),
        Product(title: "Hoodie #3", price: "$32", imageName: "hoodie03.png"),
        Product(title: "Hoodie #4", price: "$32", imageName: "hoodie04.png")
    ]
    
    private let shirts = [
        Product(title: "T-shirt #1", price: "$18", imageName: "shirt01.png"),
        Product(title: "T-shirt #2", price: "$19", imageName: "shirt02.png"),
        Product(title: "T-shirt #3", price: "$18", imageName: "shirt03.png"),
        Product(title: "T-shirt #4", price: "$18", imageName: "shirt04.png"),
        Product(title: "T-shirt #5", price: "$18", imageName: "shirt05.png")
    ]
    
    private let digitalGoods = [Product]()
    
    func getProducts(forCategoriesTitle title: String) -> [Product] {
        
        switch title {
        case "SHIRTS":
            return getShirts()
        case "HATS":
            return getHats()
        case "HOODIES":
            return getHoodies()
        case "DIGITAL":
            return getDigitalGoods()
        default:
            return getShirts()
        }
    }
    
    func getCatetories() -> [Category]{
        return catetories
    }
    
    func getHats() -> [Product]{
        return hats
    }
    
    func getHoodies() -> [Product] {
        return hoodies
    }
    
    func getShirts() -> [Product] {
        return shirts
    }
    
    func getDigitalGoods() -> [Product] {
        return digitalGoods
    }
    
}
